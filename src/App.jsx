import './App.css'
import { Canvas } from '@react-three/fiber'
import Experience from '../components/home.jsx'

function App() {

  return (
    <div className="App">
      <Canvas 
        camera={ {
          fov: 45,
          near: 0.1,
          far: 200,
          position: [0, 0.25, 20]
        } }
      >
        <Experience />
      </Canvas>
    </div>
  )
}

export default App

import { OrbitControls, Center, Float, Text3D } from '@react-three/drei'

// Check out https://github.com/pmndrs/drei for documentation on drei.

export default function Experience() {
  return <>

    <OrbitControls />

    <Center>
      <Float
        speed={1}
        floatIntensity={1}
        rotationIntensity={0.25}
      >
        <Text3D
          font="../public/helvetiker_regular.typeface.json"
          size={0.75}
          height={0.2}
          curveSegments={12}
          bevelEnabled
          bevelThickness={0.02}
          bevelSize={0.02}
          bevelOffset={0}
          bevelSegments={5}
        >
          MANY WORLDS LARGE ENSEMBLE
          <meshNormalMaterial />
        </Text3D>
      </Float>
    </Center>

    <directionalLight position={[1, 2, 3]} intensity={1.5} />
    <ambientLight intensity={0.5} />

    <mesh position-y={- 2} rotation-x={- Math.PI * 0.5} scale={100}>
      <planeGeometry />
      <meshStandardMaterial color="black" />
    </mesh>

  </>
}
